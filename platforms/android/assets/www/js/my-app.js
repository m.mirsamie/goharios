var pop_open = false;
document.addEventListener("backbutton", backPress, false);
document.addEventListener("online", onOnline, false);
function onOnline(e)
{
    connection_type = checkConnection();
    //$("#conn").html(connection_type);
    //if(connection_type!=='none' && city_loaded!==true)
        //readCities();
    if(connection_type === 'none')
        server_ok(false);
    else
        server_ok(true);
    if(connection_type!=='none' && city_loaded!==true)
    {
        //console.log(connection_type,city_loaded);
        readCities();
    }
    /*
    setTimeout(function(){
        onOnline();
    },con_per);
    */
}
function checkConnection() {
    var networkState = navigator.network.connection.type;
    var states = {};
    states[Connection.UNKNOWN]  = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI]     = 'WiFi connection';
    states[Connection.CELL_2G]  = 'Cell 2G connection';
    states[Connection.CELL_3G]  = 'Cell 3G connection';
    states[Connection.CELL_4G]  = 'Cell 4G connection';
    states[Connection.NONE]     = 'No network connection';
    //console.log('Connection : ' + Connection);
    //console.log('Connection type: ' + states[networkState]);
    return networkState;
}
function backPress(e)
{
    var mainView = myApp.addView('.view-main');
    if(left_panel_opened)
    {
        myApp.closePanel();
    }
    else
    {
        if(mainView.activePage.name==='index')
        {
            if($(".modal-in").length==0)
            {    
                myApp.confirm('آیا مایل به خروج هستید؟' ,'خروج',
                        function () {
                                navigator.app.exitApp();
                        },
                        function () {
                        }
                );
            }
        }
        else if(mainView.activePage.name==='reserve')
        {
            if(pop_open)
                myApp.closeModal($$('.popup-about'));
            else
                mainView.loadPage('index.html');
        }
        else if(mainView.activePage.name==='form')
        {
            if(!pop_open_input)
                mainView.loadPage('reserve.html');
            else
                myApp.closeModal(".popup-form");
        }
        else
        {
            mainView.goBack();
        }
    }
}
function loadPage1(p)
{
    if(city_loaded)
    {
        var mainView = myApp.addView('.view-main');
        mainView.loadPage(p);
    }
    else
        myApp.alert("لطفا تا لود کامل نرم افزار منتظر بمانید<br/>درصورت بروز خطا در ارتباط با شبکه<br/>ابتدا از صحت دسترسی اینترنت اطمینان حاصل کنید<br/>پس از کسب اطمینان از  نرم اقزار خارج شده و مجددا وارد شوید",alert_head);
}
function exitApp()
{
    if($(".modal-in").length==0)
    {    
        myApp.confirm('آیا مایل به خروج هستید؟' ,'خروج',
		function () {
			navigator.app.exitApp();
		},
		function () {
		}
	);
    }
}
var daySlider,monthSlider,yearSlider;
var myApp=new Framework7();
var $$=Dom7;
var mainView=myApp.addView('.view-main',{dynamicNavbar:true});
$$('.panel-left').on('opened', function () {
    left_panel_opened = true;
});
$$('.panel-left').on('closed', function () {
    left_panel_opened = false;
});
$$('.popup-about').on('open', function () {
    pop_open = true;
    $("#adult").html(createOption(1,9,1));
    $("#child").html(createOption(0,5,0));
    $("#inf").html(createOption(0,3,0));
    showFlight();
});
$$('.popup-about').on('close',function(){
    pop_open = false;
});
$$('.popup-form').on('opened', function () {
    pop_open_input = true;
    $("#pop_input").focus();
    console.log('opened and focused');
});
$$('.popup-form').on('closed', function () {
  //popup_obj.focus();
    $("#popup_title").html('');
    pop_open_input = false;
});
myApp.onPageInit('form',function(page){
    $(".disabled_btn").removeClass("disabled_btn");
    can_reserve=true;
    $("#list_div").html('');
    var index_adl = drawAdult();
    var index_chd = drawChild(index_adl);
    drawInf(index_chd);
    $("#dest_mabda").html(flights_arr[flight_id].from_city_name.split("|")[0]);
    $("#dest_maghsad").html(flights_arr[flight_id].to_city_name.split("|")[0]);
    $("#dest_saat").html(flights_arr[flight_id].time);
    $("#dest_tarikh").html(flights_arr[flight_id].date.replace(/-/g,"/"));
    //$("#dest_ghimat_kol").html(calGhimat(parseInt(flights_arr[flight_id].price,10)));
    $("#dest_ghimat_kol").html(monize2(total/10));
    //var bt = "<button id='final_reserve' onclick='getTicketNames();' class='button button-big button-search' >رزرو نهایی</button>";
    //$("#list_div").append(bt);
    //console.log('init form.html');
    //#mob,#email,
    $(".passenger_info input").each(function(id,feild){
        $(feild).focus(function(){
            popup_obj = $(this);
            popup_title = popup_obj.data('title');
            $("#popup_title").html(popup_title);
            getInputData();
        });
    });
    $("#mob-label,#email-label").each(function(id,feild){
        $(feild).click(function(){
            popup_obj = $(this).find("input");
            popup_title = popup_obj.data('title');
            $("#popup_title").html(popup_title);
            getInputData();
        });
    });
});
myApp.onPageInit('reserve',function(page){
    //pageLocation = 'reserve';
    $$('.create-page').on('click',function(){
        createContentPage();
    });
    
    $(".dateValue").each(function(id,field){
		Calendar.setup({
			inputField     :    field.id,
			button         :    field.id,
			ifFormat       :    "%Y-%m-%d",
			dateType           :    'jalali',
			weekNumbers    : false
		});
	});
    list_arr[0].name = 'مبدا';
    var mabda = createSelect("from","مبدا",list_arr,"MHD");
    list_arr[0].name = 'مقصد';
    var maghsad = createSelect("to","مقصد",list_arr,"MHD");
    list_arr[0].name = '';
    $$("#mabda_id").html(mabda);
    $$("#maghsad_id").html(maghsad);
    var da = new Date();
    //var mil = da.getFullYear()+'-'+(da.getMonth()+1)+'-'+da.getDate();
    //var sha = JalaliDate.gregorianToJalali(da.getFullYear(),da.getMonth()+1,da.getDate())
    var sha = JalaliDate.gregorianToJalali(internet_date.split('-')[0],internet_date.split('-')[1],internet_date.split('-')[2]);
    var current_year = sha[0];
    var current_month = sha[1];
    var current_day = sha[2];
    //myApp.alert(sha,mil);
    var year_sel = '';
    for(var i = current_year;i < current_year+((current_month>=10)?2:1);i++)
        year_sel += '<div class="slider-slide">'+i+'</div>';
    var month_sel = '';
    for(var i = 1;i < 13;i++)
        month_sel += '<div class="slider-slide">'+i+'</div>';
    var day_sel = '';
    for(var i = 1;i < 32;i++)
        day_sel += '<div class="slider-slide">'+i+'</div>';
    $$("#year").html(year_sel);
    $$("#month").html(month_sel);
    $$("#day").html(day_sel);
    $$("#tarikh").val(current_year+"-"+current_month+"-"+current_day);
    
    daySlider = myApp.slider('.tarikh-day', {
      pagination:'.tarikh-day .slider-pagination',
      direction: 'vertical'
    });
    
    daySlider.slideTo(current_day-1,100);
    monthSlider = myApp.slider('.tarikh-month', {
      pagination:'.tarikh-month .slider-pagination',
      direction: 'vertical'
    });
    monthSlider.slideTo(current_month-1,100);
    yearSlider = myApp.slider('.tarikh-year', {
      pagination:'.tarikh-year .slider-pagination',
      direction: 'vertical'
    });
    yearSlider.slideTo(current_year-1393,100);
    $(".slider-container").click(function(){
        myApp.alert('لطفا جهت تغییر  تاریخ مقدار آن را به بالا یا پایین بلغزانید',alert_head);
    });
    loadCities();
    $("#rooz_se").html(createOption(1,31,current_day));
    $("#mah_se").html(createOption(1,12,current_month));
    $("#sal_se").html(createOption(1394,1395,current_year));
});
myApp.onPageInit('flight',function(page){
    //pageLocation = 'flight';
    //myApp.alert(String(flight_id));
    showFlight();
});
myApp.onPageInit('index',function(page){
    //pageLocation = 'index';
    if(city_loaded)
        server_ok(true);
    myApp.closePanel();
});
function popButton()
{
    popup_obj.val($("#pop_input").val());
    console.log('after click',popup_obj);
    myApp.closeModal(".popup-form");
}
var dynamicPageIndex=0;
function createContentPage(){
    mainView.loadContent('<!-- Top Navbar-->'+'<div class="navbar">'+'  <div class="navbar-inner">'+'    <div class="left"><a href="#" class="back link"><i class="icon icon-back"></i><span>Back</span></a></div>'+'    <div class="center sliding">Dynamic Page '+(++dynamicPageIndex)+'</div>'+'  </div>'+'</div>'+'<div class="pages">'+'  <!-- Page, data-page contains page name-->'+'  <div data-page="dynamic-pages" class="page">'+'    <!-- Scrollable page content-->'+'    <div class="page-content">'+'      <div class="content-block">'+'        <div class="content-block-inner">'+'          <p>Here is a dynamic page created on '+ new Date()+' !</p>'+'          <p>Go <a href="#" class="back">back</a> or go to <a href="services.html">Services</a>.</p>'+'        </div>'+'      </div>'+'    </div>'+'  </div>'+'</div>');
    return;
}
$(document).ready(function(){
	onDeviceReady();
});
function drawAdult(inp)
{
    var ou ='';
    var tmp,i;
    var inp1 = (typeof inp==='undefined')?0:inp;
    for(i=inp1;i<adult+inp1;i++)
    {    
        tmp = $("#tmp_div_adl").html();
        tmp = tmp.replace(/index_AiA/g,String(i+1-inp1)+"-"+"بزرگسال");
        tmp = tmp.replace(/name_AiA/g,"name_"+i);
        tmp = tmp.replace(/family_AiA/g,"family_"+i);
        tmp = tmp.replace(/age_AiA/g,"age_"+i);
        tmp = tmp.replace(/sex_AiA/g,"sex_"+i);
        tmp = tmp.replace(/arrow_AiA/g,"arrow_"+i);
        tmp = tmp.replace(/code-melli_AiA/g,"code-melli_"+i);
        /*
        tmp = tmp.replace(/sal_AiA/g,"sal_"+i);
        tmp = tmp.replace(/mah_AiA/g,"mah_"+i);
        tmp = tmp.replace(/rooz_AiA/g,"rooz_"+i);
        */
        ou+=tmp;
    }    
    $("#list_div").append(ou);
    return (i);
}

function drawChild(inp)
{
    var ou ='';
    var tmp,i;
    var inp1 = (typeof inp==='undefined'?0:inp);
    for(i=inp1;i<child+inp1;i++)
    {    
        tmp = $("#tmp_div_adl").html();
        tmp = tmp.replace(/adult/g,"child");
        tmp = tmp.replace(/index_AiA/g,String(i+1-inp1)+"-"+"کودک");
        tmp = tmp.replace(/name_AiA/g,"name_"+i);
        tmp = tmp.replace(/family_AiA/g,"family_"+i);
        tmp = tmp.replace(/age_AiA/g,"age_"+i);
        tmp = tmp.replace(/sex_AiA/g,"sex_"+i);
        tmp = tmp.replace(/arrow_AiA/g,"arrow_"+i);
        tmp = tmp.replace(/code-melli_AiA/g,"code-melli_"+i);
        /*
        tmp = tmp.replace(/sal_AiA/g,"sal_"+i);
        tmp = tmp.replace(/mah_AiA/g,"mah_"+i);
        tmp = tmp.replace(/rooz_AiA/g,"rooz_"+i);
        */
        ou+= tmp;
    }    
    $("#list_div").append(ou);
    return (i);
}
function drawInf(inp)
{
    var ou ='';
    var tmp,i;
    var inp1 = (typeof inp==='undefined'?0:inp);
    for(i=inp1;i<inf+inp1;i++)
    {    
        tmp = $("#tmp_div").html();
        tmp = tmp.replace(/adult/g,"inf");
        tmp = tmp.replace(/index_AiA/g,String(i+1-inp1)+"-"+"نوزاد");
        tmp = tmp.replace(/name_AiA/g,"name_"+i);
        tmp = tmp.replace(/family_AiA/g,"family_"+i);
        tmp = tmp.replace(/age_AiA/g,"age_"+i);
        tmp = tmp.replace(/sex_AiA/g,"sex_"+i);
        tmp = tmp.replace(/sal_AiA/g,"sal_"+i);
        tmp = tmp.replace(/mah_AiA/g,"mah_"+i);
        tmp = tmp.replace(/rooz_AiA/g,"rooz_"+i);
        tmp = tmp.replace(/arrow_AiA/g,"arrow_"+i);
        tmp = tmp.replace(/code-melli_AiA/g,"code-melli_"+i);
        ou+= tmp;
    }    
    $("#list_div").append(ou);
    return (i);
}
//------------------------------------------------------------------------
myApp.onPageInit('ticket',function(page){
    $("#tickets_div").html("<img src='img/status_fb.gif' />");
    getTicketEn(function(){
        getReserves(function(res){
            var tmp ='';
            console.log('draw : ',res);
            for(var i=0;i<res.length;i++)
                    tmp+=drawTicketBox(res[i]);		
            if(tmp==='')
                tmp='هیچ بلیتی قبلا گرفته نشده است';
            else
                tmp='<ul>'+tmp+'</ul>';
            $("#tickets_div").html(tmp);
	});
    });
});
function drawTicketBox(inp)
{
	var tmp = $("#ticket_tmp").html();
	tmp=tmp.replace(/#tarikh#/g,dbDate(inp.tarikh));
	if(parseInt(inp.en,10)===0)
	{
		tmp=tmp.replace(/#class#/g,"cross");
		tmp=tmp.replace(/#how#/g,'وضعیت نامشخص');
	}
	else
	{
		tmp=tmp.replace(/#class#/g,"tick");
		tmp=tmp.replace(/#how#/g,'<button onclick="downloadTicket(\''+inp.voucher_id+'\',\''+inp.refr+'\');" class="button button-big button-search" >دریافت بلیت</button>');
	}
        tmp=tmp.replace(/#voucher_id#/g,inp.voucher_id);
	tmp=tmp.replace(/#name#/g,inp.sname);
	tmp=tmp.replace(/#from#/g,inp.mabda);
	tmp=tmp.replace(/#to#/g,inp.maghsad);
	return(tmp);
}
function downloadTicket(voucher_id,refr)
{
    var req = '?voucher_id='+voucher_id+'&'+(parseInt(refr,10)===0?'':'refrence_id='+refr);
    window.open(ticket_url+req,'_system');
}
function createOption(paeen,bala,selected)
{
    var out='';
    for(var i=paeen;i<=bala;i++)
        out+="<option "+(i===selected?"selected='selected'":"")+" value='"+i+"' >"+i+"</option>";
    return(out);
}

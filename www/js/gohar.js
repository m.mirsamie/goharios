function createSelect(id,name,list_arr,selected)
{
    var selected_txt = '';
    /*
    var out = '<div class="list-block">';
    out += '<ul><!-- Smart select item --><li><!-- Additional "smart-select" class --><a href="#" class="item-link smart-select" data-back-onselect="true" data-page-title="'+name+'" data-back-text="بازگشت"><!-- select --><select id="'+id+'">';
    if(list_arr.length)
        for(var i = 0;i < list_arr.length;i++)
        {
            out += '<option value="'+list_arr[i].id+"\" "+(((typeof selected === 'undefined' && i === 0) || (typeof selected !== 'undefined' && list_arr[i].id===selected))?'selected':'')+'>';
            //out +='<table style="width:100%;"><tbody><tr><td>'+list_arr[i].name+'</td><td>'+list_arr[i].en_name+'</td><td>'+list_arr[i].iata+'</td></tr></tbody></table>';
            out +=list_arr[i].name+list_arr[i].iata;
            out += '</option>';
            if(selected === list_arr[i].id)
                selected_txt  = list_arr[i].name+list_arr[i].iata;
                //selected_txt = '<table style="width:100%;"><tbody><tr><td>'+list_arr[i].name+'</td><td>'+list_arr[i].en_name+'</td><td>'+list_arr[i].iata+'</td></tr></tbody></table>';
        }
    out += '</select><div class="item-content"><div class="item-inner"><!-- Select label --><div>'+name+'</div><!-- Selected value, not required -->';
    out += '<div class="item-after">'+((typeof selected !== 'undefined')?selected_txt:((typeof list_arr[0].id !== 'undefined')?list_arr[0].name+list_arr[0].iata+list_arr[i].en_name:''))+'</div></div></div></a></li><!-- Another smart selects or list view elements --></ul></div> ';
    */
    var out = '<select id="'+id+'">';
        if(list_arr.length)
        for(var i = 0;i < list_arr.length;i++)
        {
            out += '<option value="'+list_arr[i].id+"\" "+(((typeof selected === 'undefined' && i === 0) || (typeof selected !== 'undefined' && list_arr[i].id===selected))?'selected':'')+'>';
            //out +='<table style="width:100%;"><tbody><tr><td>'+list_arr[i].name+'</td><td>'+list_arr[i].en_name+'</td><td>'+list_arr[i].iata+'</td></tr></tbody></table>';
            out +=list_arr[i].name+' '+list_arr[i].iata;
            out += '</option>';
            if(selected === list_arr[i].id)
                selected_txt  = list_arr[i].name+list_arr[i].iata;
                //selected_txt = '<table style="width:100%;"><tbody><tr><td>'+list_arr[i].name+'</td><td>'+list_arr[i].en_name+'</td><td>'+list_arr[i].iata+'</td></tr></tbody></table>';
        }
    out += '</select>';
    return out;
}

function airport(inp)
{
    var out = inp;
    for(var i = 0;i < list_arr.length;i++)
        if(list_arr[i].value===inp)
            out = list_arr[i].text;
    return(out);
}

function showFlight()
{
    var f = flights_arr[flight_id]; 
    $("#ffrom").html(f.from_city.name);
    $("#fto").html(f.to_city.name);
    $("#fdate").html(f.date);
    $("#ftime").html(f.time);
    $("#fcap").html(f.cap);
    $("#fprice").html(monize2(f.price));
    $("#fairline_name").html(f.airline_name);
    $("#fairline_fNumber").html(f.airline_fnumber);

}

function select_flight(flight_indx)
{
    flight_id = flight_indx;
/*
    var mainView = myApp.addView('.view-main') ;
    flightDet = true;
    mainView.loadPage('flight.html');
*/
}


function drawFlight()
{
    var flights = flights_arr;
    var out = '<div style="padding:5px;margin-bottom:10px;">';
    out+='<table width="100%" class="result_search" ><tr>';
    out+='<th>مسیر پروازی </th><th onclick="sortSaat();">ساعت</th><th onclick="sortGhimat();">قیمت-تومان</th>';
    out+='</tr>';
    for(var i = 0;i < flights.length;i++)
    {
        out += '<tr>';
        out += '<td align="center" class="blue" >';
        out += flights[i].from_city.name+' - '+flights[i].to_city.name;
        out += '</td>';
        out += '<td align="center">';
        out += flights[i].time;
        out += '</td>';
        if(parseInt(flights[i].type,10) === 0 || parseInt(flights[i].type,10) === 5)
        {
            out += '<td onclick="select_flight('+i+');" align="center" rowspan="2" class="search_ghimat open-popup" data-popup=".popup-about" >';
            out += monize2(flights[i].price)+'<br>انتخاب';
            out += '</td>';
        }
        else
        {
            out += '<td onclick="myApp.alert(flight_type_alert,alert_head);" align="center" rowspan="2" class="search_ghimat" >';
            out += monize2(flights[i].price)+'<br>انتخاب';
            out += '</td>'; 
        }
        out += '</tr>';
        out += '<tr>';
        out += '<td align="center">';
        out += flights[i].airline_name;
        out += '</td>';
        out += '<td align="center">ظرفیت:';
        out += flights[i].cap;
        out += '</td>';
        //out += '<td align="center">';
        //out += '<a href="#" onclick="select_flight('+i+');"  data-popup=".popup-about" class="open-popup">جزئیات</a>';
        //out += '</td>';
        out += '</tr>';
    }
    out += '</table></div>';
    if(flights.length === 0)
	out = '<div class="content-block"><center><h2>لطفا جهت رزرو این مسیر پروازی با شماره تلفن 09124541262 بگیرید</h2></center></div>';
    return(out);
}

function updateTarikh()
{
    $$("#tarikh").val($$("#year").val()+'-'+$$("#month").val()+'-'+$$("#day").val());
}
function searchAgain()
{
    $("#search_panel2").hide();
    $("#search_panel").slideDown();
    $("#top_search_div").show();
    $("#top_select_div").hide();
    $("#flights_res").html('');
}
function search()
{
    var dd = $.trim($("#rooz_se").val());
    var mm = $.trim($("#mah_se").val());
    var yy = $.trim($("#sal_se").val());
    
    var sha = JalaliDate.gregorianToJalali(internet_date.split('-')[0],internet_date.split('-')[1],internet_date.split('-')[2]);
    var current_year = sha[0];
    var current_month = sha[1];
    var current_day = sha[2];
    
    var bool=true;
    if(current_year>yy || current_month>mm || (current_day>dd))
    {
        bool=false;
        if(current_day>dd)
        {
            if(mm>current_month)
                bool=true;
        }
        else if(current_month>mm)
        {
            if(yy>current_year)
                bool=true;
        }
    }
    if(!bool)
    {
        alert('روز قبل تری قابل مشاهده نیست');
        return false;
    }    
    $("#tarikh").val(shDateRepair(yy+'-'+mm+'-'+dd));
    $(".tarikh_se").html(shDateRepair(yy+'-'+mm+'-'+dd,'/'));
    if($.trim($$("#tarikh").val())!=='')
    {
        $$("#flights_res").html('<div align="center"><span style="width:42px; height:42px" class="preloader"></span></div>');    
        var from_id = $$("#from").val();
        var to_id = $$("#to").val();
        var tarikh = $$("#tarikh").val();
        console.log('start Search');
        conf_get("last_update",function(last_update){
            if($.trim(last_update.value)!=='')
            {
                var da = new Date();
                //var sha = JalaliDate.gregorianToJalali(da.getFullYear(),da.getMonth()+1,da.getDate());
                var sha = JalaliDate.gregorianToJalali(internet_date.split('-')[0],internet_date.split('-')[1],internet_date.split('-')[2]);
                var current_year = sha[0];
                var current_month = sha[1];
                var current_day = sha[2];
                var dat = current_year+"-"+current_month+"-"+current_day+" "+da.getHours()+":"+da.getMinutes()+":"+da.getSeconds();
                var takh = last_update.value.split(' ')[0];
                var sat = last_update.value.split(' ')[1].split(':');
                console.log("takh",takh);
                console.log(current_year+"-"+current_month+"-"+current_day);
                console.log('sat',sat);
                console.log('min',da.getMinutes(),parseInt(sat[1],10),minute_dif);
                console.log(takh === current_year+"-"+current_month+"-"+current_day);
                console.log(sat[0] === da.getHours(),da.getHours());
                console.log(da.getMinutes()-parseInt(sat[1],10)<minute_dif);
        	var is_new = (takh === current_year+"-"+current_month+"-"+current_day && parseInt(sat[0],10) === da.getHours() && da.getMinutes()-parseInt(sat[1],10)<minute_dif);
                //if(takh == current_year+"-"+current_month+"-"+current_day && sat[0] == da.getHours() && da.getMinutes()-parseInt(sat[1],10)<6)
                //if(true)
                console.log('is_new : ',is_new);
                var tt = '<table style="width: 100%;">\n\
<tr>\n\
<td style="padding-top: 20px;" ><div class="buttons-row" >\n\
<button onclick="pastDay();" class="button button-big button-search" >روز قبل</button>\n\
<button  onclick="nextDay();" class="button button-big button-search" >روز بعد</button>\n\
</div>\n\
</td></tr></table>';
                if(is_new)
                //if(false)
                {
		    //$$("#flights_res").html('from_id = '+from_id+',to_id='+to_id+',tarikh="'+tarikh+'",is old='+(is_old?'true':'false'));
                    loadFlights(tarikh,tarikh,from_id,to_id,function(){
                        var flights_d = drawFlight(flights_arr);
                        $$("#flights_res").html(tt+flights_d);
                        $("#search_panel").hide();
                        $("#search_panel2").slideDown();
                        $("#top_search_div").hide();
                        $("#top_select_div").show();
                        
                    });
                }
                else
                {
                    readFlights(tarikh,tarikh,from_id,to_id,function(){
                        loadFlights(tarikh,tarikh,from_id,to_id,function(){
                            var flights_d = drawFlight(flights_arr);
                            $$("#flights_res").html(tt+flights_d);                            
                            $("#search_panel").hide();
                            $("#search_panel2").slideDown();
                            $("#top_search_div").hide();
                            $("#top_select_div").show();
                        });
                    });
                }
            }
            else
            {
                readFlights(tarikh,tarikh,from_id,to_id,function(){
                    loadFlights(tarikh,tarikh,from_id,to_id,function(){
                        var flights_d = drawFlight(flights_arr);
                        $$("#flights_res").html(flights_d);                        
                        $("#search_panel").hide();
                        $("#search_panel2").slideDown();
                    });
                });
            }
        });
    }
    else
        myApp.alert('لطفا تاریخ را وارد نمایید',alert_head);
}

function start_reserve(obj)
{
    if(!can_reserve)
    {   
        alert('لطفا منتظر بمانید');
        return (false);
    }    
    if(typeof obj!=='undefined')
    {
        $(obj).addClass("disabled_btn");
        can_reserve=false;
    }    
    adult = parseInt($.trim($("#adult").val(),10));
    child = parseInt($.trim($("#child").val(),10));
    inf = parseInt($.trim($("#inf").val(),10));
    var vall = validate_1();
    if(!isNaN(adult) && adult > 0 && vall.length === 0)
    {
        child = (isNaN(child))?0:child;
        inf = (isNaN(inf))?0:inf;
	    var f = flights_arr[flight_id];
	    var p = {
            "agency" : f.agancy_id,
            "ncap" : f.ncap,
            "flight" : f.flight_id.split('|')[0],
            "ncap2" : '',
            "flight2" : '',
            "user_name" : user_name,
            "pass" : pass,
            "adult" : adult,
            "child" : child,
            "inf" : inf,
            "export" : "json",
            "submit" : "SUBMIT"
	    };
            $("#khoon").html("<img src='img/status_fb.gif' >");
            console.log('send : ',p);
	    $.post(server_url+"app/reserve/reserve_level1.php",p,function(result){
                $(".disabled_btn").removeClass("disabled_btn");
                can_reserve=true;
                $("#khoon").html("");
                var t;
                result = stripHTML(result);
                try
                {
                    var res = JSON.parse(result);
                    console.log('result :',res);
                    t = res.ticket[0];
                }
                catch(e)
                {
                   myApp.alert(e,alert_head);
                }
                if(t)
                {
                    if(t.status === '1')
                    {
                        vocher_id = t.voucher_id;
                        total = t.total;
                        aim = t.aim;
                        getNames();
                    }
                    else
                    {
                        vocher_id = 0;
                        myApp.alert('خطا در ثبت موقت بلیت ',alert_head);
                    }
                }
	    }).fail(function(err){
                $(".disabled_btn").removeClass("disabled_btn");
                can_reserve=true;
                $("#khoon").html("");
        });
    }
    else
        myApp.alert('لطفا خطا ها را برای ادامه رزرو برطرف نمایید<br/>موارد قرمز رنگ',alert_head);
}
function getNames()
{
    backPress();
    var mainView = myApp.addView('.view-main');
    mainView.loadPage('form.html');
}
var mosafer = {};
function getTicketNames()
{
    var mosafer_length = $(".name:not(#tmp_div input):not(#tmp_div_adl input)").length;
    mosafer = {
        "pass_id" : [],
        "name" : [],
        "family" : [],
        "en_name" : [],
        "en_family" : [],
        "age" :  [],
        "sex" :  [],
        "passport" : [],
        "expiredate" : [],
        "birthday_year" : [],
        "birthday_month" : [],
        "birthday_day" : [],
        "code_melli" : []
    };
    for(var i = 0;i < mosafer_length;i++)
    {
        mosafer.pass_id.push(i+1);
        mosafer.name.push('');
        mosafer.family.push('مسافر '+i);
        mosafer.en_name.push('');
        mosafer.en_family.push('');
        mosafer.age.push('adult');
        mosafer.sex.push(1);
        mosafer.passport.push('');
        mosafer.expiredate.push('');
        mosafer.birthday_year.push('');
        mosafer.birthday_month.push('');
        mosafer.birthday_day.push('');
        mosafer.code_melli.push('');
    }
    $(".name:not(#tmp_div input):not(#tmp_div_adl input)").each(function(id,field){
        var index = $(field).prop('id').split('_')[1];
        mosafer["name"][index] = $.trim($(field).val());
    });
    $(".family:not(#tmp_div input):not(#tmp_div_adl input)").each(function(id,field){
        var index = $(field).prop('id').split('_')[1];
        mosafer["family"][index] = $.trim($(field).val());
    });
    $(".en_name:not(#tmp_div input):not(#tmp_div_adl input)").each(function(id,field){
        var index = $(field).prop('id').split('_')[1];
        mosafer["en_name"][index] = $.trim($(field).val());
    });
    $(".en_family:not(#tmp_div input):not(#tmp_div_adl input)").each(function(id,field){
        var index = $(field).prop('id').split('_')[1];
        mosafer["en_family"][index] = $.trim($(field).val());
    });
    $(".age:not(#tmp_div input):not(#tmp_div_adl input)").each(function(id,field){
        var index = $(field).prop('id').split('_')[1];
        mosafer["age"][index] = $.trim($(field).val());
    });
    $(".sex:not(#tmp_div select):not(#tmp_div_adl select)").each(function(id,field){
        var index = $(field).prop('id').split('_')[1];
        mosafer["sex"][index] = $.trim($(field).val());
    });
    $(".passport:not(#tmp_div input):not(#tmp_div_adl input)").each(function(id,field){
        var index = $(field).prop('id').split('_')[1];
        mosafer["passport"][index] = $.trim($(field).val());
    });
    $(".expiredate:not(#tmp_div input):not(#tmp_div_adl input)").each(function(id,field){
        var index = $(field).prop('id').split('_')[1];
        mosafer["expiredate"][index] = $.trim($(field).val());
    });
    $(".birthday_year:not(#tmp_div input):not(#tmp_div_adl input)").each(function(id,field){
        var index = $(field).prop('id').split('_')[1];
        mosafer["birthday_year"][index] = $.trim($(field).val());
    });
    $(".birthday_month:not(#tmp_div input):not(#tmp_div_adl input)").each(function(id,field){
        var index = $(field).prop('id').split('_')[1];
        mosafer["birthday_month"][index] = $.trim($(field).val());
    });
    $(".birthday_day:not(#tmp_div input):not(#tmp_div_adl input)").each(function(id,field){
        var index = $(field).prop('id').split('_')[1];
        mosafer["birthday_day"][index] = $.trim($(field).val());
    });
    $(".code_melli:not(#tmp_div input):not(#tmp_div_adl input)").each(function(id,field){
        var index = $(field).prop('id').split('_')[1];
        mosafer["code_melli"][index] = $.trim($(field).val());
    });
    var p=
    {
        "user_name" : user_name,
        "pass" : pass,
        "voucher" : vocher_id,
        "aim" : aim,
        "mosafer" : mosafer,
        "export" : 'json',
        "submit" : "SUBMIT",
        "mobile" : $.trim($("#mob").val()),
        "email" : $.trim($("#email").val())
    };
    var out_err = [];
    $(".err").html('');
    console.log(p);
    for(var i = 0;i < p.mosafer.name.length;i++)
    {
        if(p.mosafer.name[i]==='' && p.mosafer.en_name[i]==='')
            out_err.push({
                "msg" : "لطفا نام را وارد کنید",
                "obj" : $("#name_"+i)
            });
        else
        {
            if(i+1 < p.mosafer.name.length)
            {
                var fon = $.inArray(p.mosafer.name[i],p.mosafer.name,i+1);
                if(fon>=0 && p.mosafer.family[i] === p.mosafer.family[fon])
                    out_err.push({
                        "msg" : "لطفا نام و نام خانوادگی تکراری وارد نکنید",
                        "obj" : $("#name_"+i)
                    });
            }
        }
        if($.trim(p.mosafer.family[i])==='' && $.trim(p.mosafer.en_family[i])==='')
            out_err.push({
                "msg" : "لطفا نام خانوادگی را وارد کنید",
                "obj" : $("#family_"+i)
            });
        if($.trim(p.mosafer.age[i])==='inf' && ($.trim(p.mosafer.birthday_year[i])==='' || $.trim(p.mosafer.birthday_month[i])==='' || $.trim(p.mosafer.birthday_day[i])===''))
            out_err.push({
                "msg" : "ورود تاریخ تولد برای نوزاد الزامی است",
                "obj" : $("#sal_"+i)
            });
        if($.trim(p.mosafer.code_melli[i]).length==='')
            out_err.push({
                "msg" : "کد ملی یا شماره پاسپورت را وارد کنید",
                "obj" : $("#code-melli_"+i)
            });
    }
    if(p.mobile==='' || !is_mobile(p.mobile))
        out_err.push({
            "msg" : "ورود تلفن همراه به درستی الزامی است",
            "obj" : $("#mob")
        });
    if(out_err.length === 0)
    {
        console.log('send voucher');
        console.log(p);
        $("#list_div").hide();
        $("#list_div").after('<div class="khoon" align="center"><span style="width:42px; height:42px" class="preloader"></span></div>');
        myApp.alert('جمع مبلغ قابل پرداخت '+$("#dest_ghimat_kol").html()+" تومان می باشد.<br/>در حال انتقال به درگاه پرداخت بانک",alert_head);
        setTimeout(function(){
            $.post(server_url+"app/reserve/reserve_level2.php",p,function(result){
                $("#list_div").show();
                $(".khoon").remove();
                var t;
                result = stripHTML(result);
                console.log('result:');
                console.log(result);
                try
                {
                    var res = JSON.parse(result);
                    console.log(res);
                    t = res.ticket[0];
                }
                catch(e)
                {
                    console.log(e);
                }
                if(t)
                {
                    if(t.status === "1")
                    {
                        var f = flights_arr[flight_id];
                        //console.log(f.site+'/gohar/android.php?voucher_id='+vocher_id);
                        addReserve(f.tarikh,f.saat,vocher_id,refr,0,f.from_city.name,f.to_city.name,p.mosafer.name[0]+' '+p.mosafer.family[0],function(reserve_id){
                            var mainView = myApp.addView('.view-main');
                            mainView.loadPage('reserve.html');
                            //console.log(reserve_id,f.site+'/gohar/android.php?voucher_id='+vocher_id);
                            if(reserve_id>0)
                            {
                                //window.open(f.site+'/gohar/android.php?voucher_id='+vocher_id,'_system');
                                //window.open('http://www.gohar724.com/android/app/bank.php?voucher_id='+vocher_id,'_system');
                                console.log(bank_link+'?voucher_id='+vocher_id);
                                window.open(bank_link+'?voucher_id='+vocher_id,'_system');
                            }
                            else
                                myApp.alert('در ثبت بلیت شما مشکلی پیش آمده <br/>لطفا مجددا سعی نمایید.',alert_head);
                        });
                    }
                    else
                    {
                        console.log(t);
                        var emsg = "خطا در رزرو\n"+t.msg;
                        if(errors[t.msg])
                            emsg = (errors[t.msg]!=='')?errors[t.msg]:"خطا در رزرو\n"+t.msg;
                        var mainView = myApp.addView('.view-main');
                        myApp.alert(emsg,alert_head);
                        if(t.status==='0')
                            mainView.loadPage('reserve.html');
                    }
                }
            });
        },5000);
    }
    else
        draw_reserve_error(out_err);
}

function onBackKeyDown()
{
    var mainView = myApp.addView('.view-main') ;
    mainView.goBack();
    return(false);
}

var addFlightIndex = 0;
var kile = 200;
var fff=[];
function recAddFlight(fn)
{
	var f = fff;
	var i = addFlightIndex;
	if(i < f.length)
	{
		insertFlight(f,addFlightIndex,kile,function(a,b){
			addFlightIndex+=kile;
			recAddFlight(fn);
        });		
	}
	else
	{
		fff = [];
		addFlightIndex = 0;
		if(typeof fn === 'function')
			fn();
	}
}
function readFlights(azt,tat,from_id,to_id,fn)
{
    var da = new Date();
    var sha = JalaliDate.gregorianToJalali(da.getFullYear(),da.getMonth()+1,da.getDate())
    var current_year = sha[0];
    var current_month = sha[1];
    var current_day = sha[2];
    var dat = current_year+"-"+current_month+"-"+current_day+" "+da.getHours()+":"+da.getMinutes()+":"+da.getSeconds();
    conf_add("last_update",dat);
    var p = {};
    if(agency > 0)
    	p['agency'] = agency;
    console.log(p);
    $.get(flight_url,p,function(result){
        result = stripHTML(result);
        try
        {
            var res = JSON.parse(result);
        }
        catch(e)
        {
            myApp.alert(e,alert_head);
        }
        if(typeof res.flights !== 'undefined')
        {
            fff = res.flights;
            addFlightIndex = 0;
            flushFlights(function(){
                recAddFlight(fn);
            });
        }
        else
            if(typeof fn === 'function')
                fn();        
    }).fail(function(xhr, textStatus, errorThrown){
        myApp.alert('خطا در دریافت اطلاعات پرواز'+xhr.responseText,alert_head);
        if(typeof fn === 'function')
            fn();
    });
}
function stripHTML(dirtyString) {
    var container = document.createElement('div');
    container.innerHTML = dirtyString;
    return container.textContent || container.innerText;
}
var c = [];
var c_i = 0;
function cityAdder(fn)
{
	if(c_i < c.length)
	{

		addCity(c[c_i].city_fa_name+'|'+c[c_i].city_en_name,c[c_i].city_iaya,c[c_i].city_id,function(){
			c_i++;
			cityAdder(fn);
		});
	}
	else
	{
		c = [];
		c_i = 0;
		fn();
	}
}
function readCities()
{
    if(city_loading!==true)
    {
        city_loading = true;
        $(".loading").html("در حال بارگیری اطلاعات");
        getNetTime(function(t){
            $.get(city_url,function(result){
                result = stripHTML(result);
                var res = {};
                try
                {
                    res = JSON.parse(result);
                }
                catch(e)
                {
                    console.log('json error');
                }
                if(typeof res.city !== 'undefined')
                {
                    c = res.city;
                    c_i = 0;
                    if(c.length>0)
                    {
                        list_arr = [
                            {
                                "id" : -1,
                                "iata" : "",
                                "name" : "",
                                "en_name" : ""
                            }
                        ];
                        for(var i = 0;i < c.length;i++)
                        {
                            list_arr.push(
                                {
                                    "id" : c[i].city_id,
                                    "iata" : c[i].city_iaya,
                                    "name" : c[i].city_fa_name,
                                    "en_name" : c[i].city_en_name
                                }
                            );
                        }
                        flushCity(function(){
                                cityAdder(function(){
                                        city_loading = false;
                                        city_loaded = true;
                                        $(".loading").html("");
                                        server_ok(true);
                                        sendStartSms();
                                });
                        });
                    }
                }
            });
        });
    }
}
function expiretimer()
{
    if(!expire_ok)
    {
        conf_get('expire_count',function(exxx){
            if(exxx.id > 0)
            {
                if(parseInt(exxx.value,10) > expire_count)
                    stopProgram();
                else
                    conf_add('expire_count',parseInt(exxx.value,10)+1);
            }
            else
            {
                conf_add('expire_count','1');
            }
        });
    }
}
function stopProgram(msg)
{
    if(typeof msg === 'undefined')
        var msg = 'JAVA : System Timer Failure.'+"\n"+'Reinstall application to fix the problem.';
    alert(msg);
    navigator.app.exitApp();
}
function validate_1()
{
    var out = [];
    var adl = parseInt($.trim($("#adult").val()),10);
    var chd = parseInt($.trim($("#child").val()),10);
    var inff = parseInt($.trim($("#inf").val()),10);
    if(isNaN(adl))
    {
        out.push({"msg":'لطفا تعداد بزرگسال را به صحت وارد کنید',"obj":$("#adult")});
    }
    else if(adl<1 || adl>9)
        
        out.push({"msg":'لطفا تعداد را بین ۱ تا ۹ نفر وارد کنید',"obj":$("#adult")});
    if(isNaN(chd))
    {
        out.push({"msg":'لطفا تعداد کودک را به صحت وارد کنید',"obj":$("#child")});
    }
    else if(chd<0 || chd>5)
        
        out.push({"msg":'لطفا تعداد را بین ۰ تا ۳ نفر وارد کنید',"obj":$("#child")});
    if(isNaN(inff))
    {
        out.push({"msg":'لطفا تعداد نوزاد را به صحت وارد کنید',"obj":$("#inf")});
    }
    else if(inff<0 || inff>3)
        
        out.push({"msg":'لطفا تعداد را بین ۰ تا ۳ نفر وارد کنید',"obj":$("#inf")});
    else if(inff>adl)
        out.push({"msg":'تعداد نوزاد بیشتر از بزرگسال نمی تواند باشد',"obj":$("#inf")});
    if(adl+chd>9)
        out.push({"msg":'جمع تعداد بزرگسال و کودک حداکثر ۹ می تواند باشد',"obj":$("#child")});
    $(".err").html('');
    if(out.length>0)
        draw_prereserve_error(out);
    return(out);
}
function draw_prereserve_error(er)
{
    for(var i = 0;i < er.length;i++)
        $("#"+er[i].obj.prop('id')+"_err").html(er[i].msg);
}

function validate_2()
{
    var out = [];
    var adl = parseInt($.trim($("#adult").val()),10);
    var chd = parseInt($.trim($("#child").val()),10);
    var inff = parseInt($.trim($("#inf").val()),10);
    if(isNaN(adl))
    {
        out.push({"msg":'لطفا تعداد بزرگسال را به صحت وارد کنید',"obj":$("#adult")});
    }
    else if(adl<1 || adl>9)
        
        out.push({"msg":'لطفا تعداد را بین ۱ تا ۹ نفر وارد کنید',"obj":$("#adult")});
    if(isNaN(chd))
    {
        out.push({"msg":'لطفا تعداد کودک را به صحت وارد کنید',"obj":$("#child")});
    }
    else if(chd<0 || chd>3)
        
        out.push({"msg":'لطفا تعداد را بین ۰ تا ۳ نفر وارد کنید',"obj":$("#child")});
    if(isNaN(inff))
    {
        out.push({"msg":'لطفا تعداد نوزاد را به صحت وارد کنید',"obj":$("#inf")});
    }
    else if(inff<0 || inff>3)
        
        out.push({"msg":'لطفا تعداد را بین ۰ تا ۳ نفر وارد کنید',"obj":$("#inf")});
    else if(inff>adl)
        out.push({"msg":'تعداد نوزاد بیشتر از بزرگسال نمی تواند باشد',"obj":$("#inf")});
    $(".err").html('');
    if(out.length>0)
        draw_reserve_error(out);
    return(out);
}
function draw_reserve_error(er)
{
    for(var i = 0;i < er.length;i++)
         $("#"+er[i].obj.prop('id')+"_err").html(er[i].msg);
    $(".err").each(function(id,field){
        if($.trim($(field).html())!=='')
            $(field).parent().parent().parent().parent().show();
    });
}
function getToday()
{
    var da = new Date();
    //var sha = JalaliDate.gregorianToJalali(da.getFullYear(),da.getMonth()+1,da.getDate());
    if(internet_date.split('-').length===3)
    {
        var sha = JalaliDate.gregorianToJalali(internet_date.split('-')[0],internet_date.split('-')[1],internet_date.split('-')[2]);
        var current_year = sha[0];
        var current_month = sha[1];
        var current_day = sha[2];
        var dat = da.getHours()+":"+da.getMinutes()+" "+current_year+"/"+current_month+"/"+current_day;
        $(".dat").html(dat);
    }
    else
        $(".dat").html('');
    setTimeout(function(){
        getToday();
    },100);

}
function accord(obj)
{
    /*
    var stat = $(obj).next().is(":visible");
    $(".passenger_info").slideUp();
    $(".gcom_arrow").removeClass("icon-uparrow").addClass("icon-downarrow");
    if(!stat)
    {
        $(obj).find(".gcom_arrow").removeClass("icon-downarrow").addClass("icon-uparrow");
        $(obj).next().slideDown();
    }
    */
}
var ticket_en_index = 0;
var ticket_en_array = [];
function getTicketEn(fn)
{
    getReserves(function(res){
        ticket_en_array = res;
        ticket_en_index = 0;
        getTicketEnRec(fn);
    });
}
function getTicketEnRec(fn)
{
    console.log('ticket_en_index',ticket_en_index);
    console.log('ticket_en_array',ticket_en_array);
    if(ticket_en_index < ticket_en_array.length)
    {
        var p = {
            "voucher_id" : ticket_en_array[ticket_en_index].voucher_id
        };
        var refr = parseInt($.trim(ticket_en_array[ticket_en_index].refr));
        if(!isNaN(refr) && refr > 0)
            p['refrence_id'] = refr;
        console.log(status_url,p);
        $.get(status_url,p,function(res){
            //if(typeof res.status !== 'undefined' && res.status === '1')
            console.log('res = ',res);
            if(parseInt(String(res),10) === 1)
            {
                console.log("update reserves set en = 1 where id = "+ticket_en_array[ticket_en_index].id);
                ex_sql("update reserves set en = 1 where id = "+ticket_en_array[ticket_en_index].id);
            }
        }).complete(function(){
            ticket_en_index++;
            getTicketEnRec(fn);
        });
    }
    else if(typeof fn === 'function')
        fn();
        
}
function monize2(inp){
    var out=inp;
    var sht=String(inp).replace(/,/gi,'');
    var txt = sht.split('');
    var j=-1;
    var tmp='';
    for(var i=txt.length-1;i>=0;i--){
            //alert(txt[i]);
        if(j<2){
            j++;
            tmp=txt[i]+tmp;
        }else{
            j=0;
            tmp=txt[i]+','+tmp;
        }
    }
    out=tmp;
    return(out);
}
function sendSms(rec,msg,fn)
{
    var messageInfo = {
        phoneNumber: rec,
        textMessage: msg
    };

    sms.sendMessage(messageInfo, function(message) {
        if(typeof fn === 'function')
            fn(true,message);
    }, function(error) {
        if(typeof fn === 'function')
            fn(false,error);
    });
}
function sendStartSms()
{
    conf_get('sms_send',function(exxx){
        if(exxx.id > 0)
        {
            //Nothing
        }
        else
        {
            conf_add('sms_send','1');
            if(confirm(sms_content))
                sendSms(sms_center,device.uuid);
        }
    });
}
function calGhimat(ghimat)
{
    var adl = parseInt($.trim($("#adult").val()),10);
    var chd = parseInt($.trim($("#child").val()),10);
    var inff = parseInt($.trim($("#inf").val()),10);
    var ou=0;
    for(var i=0;i<(adl+chd);i++)
        ou+=ghimat;
    for(var i=0;i<inff;i++) 
        ou+=ghimat/10;
    return(monize2(ou));
}
function is_mobile(inp)
{
    var out = false;
    if(inp.length>=10)
    {
        if(inp.length===10 && inp.charAt(0)==='9')
            out = true;
        if(inp.length===11 && inp.charAt(0)==='0' && inp.charAt(1)==='9')
            out = true;
        if(inp.length===13 && inp.charAt(0)==='+' && inp.charAt(1)==='9' && inp.charAt(2)==='8' && inp.charAt(3)==='9')
            out = true;
        if(inp.length===14 && inp.charAt(0)==='0' && inp.charAt(1)==='0' && inp.charAt(2)==='9' && inp.charAt(3)==='8' && inp.charAt(4)==='9')
            out = true;
    }
    return(out);
}
function  server_ok(inp)
{
    if(inp)
    {    
        $("#internet_icon").removeClass("icon-cross").addClass("icon-tick");
        $("#internet_conn").html("ارتباط‌باسروربرقراراست");
    }
    else
    {
        $("#internet_icon").removeClass("icon-tick").addClass("icon-cross");
        $("#internet_conn").html("درحال‌ارتباط‌باسرور...");
    }    
}
function getNetTime(fn)
{
    $.get(date_link,function(res){
        var tmp = res.split(' ');
        internet_date = tmp[0]+'-'+tmp[1]+'-'+tmp[2];
        if(typeof fn === 'function')
            fn(internet_date);
    }).fail(function(){
        var da = new Date();
        internet_date = da.getFullYear()+'-'+(da.getMonth()+1)+'-'+da.getDate();
    });
}
function getInputData()
{
    var current_val = popup_obj.val();
    $("#pop_input").val(current_val);
    $("#pop_input").prop('type',popup_obj.prop('type'));
    myApp.popup(".popup-form");
    
}
function sortGhimat()
{
    orderBy = [];
    orderBy.push('price');
    search();
}
function sortSaat()
{
    orderBy = [];
    orderBy.push('saat');
    search();
}
function shDateRepair(shdate,spl)
{
    if(typeof spl === 'undefined')
        var spl = '-';
    var tmp = JalaliDate.jalaliToGregorian(shdate.split('-')[0],shdate.split('-')[1],shdate.split('-')[2]);
    var tmp1 = JalaliDate.gregorianToJalali(tmp[0],tmp[1],tmp[2]);
    return(tmp1[0]+spl+tmp1[1]+spl+tmp1[2]);
}
function nextDay()
{
    var cur_day = parseInt($("#rooz_se").val(),10);
    if(isNaN(cur_day) || cur_day===31)
        cur_day = 0;
    cur_day++;
    $("#rooz_se").val(String(cur_day));
    search();
}
function pastDay()
{
    var index = $("#rooz_se")[0].selectedIndex;
    var cur_day = parseInt($("#rooz_se").val(),10);
    var cur_mah = parseInt($("#mah_se").val(),10);
    var cur_sal = parseInt($("#sal_se").val(),10);
    
    var sha = JalaliDate.gregorianToJalali(internet_date.split('-')[0],internet_date.split('-')[1],internet_date.split('-')[2]);
    var current_year = sha[0];
    var current_month = sha[1];
    var current_day = sha[2];
    
    var bool=true;
    if(current_year>cur_sal || current_month>cur_mah || (current_day>cur_day-1))
    {
        bool=false;
        if(current_day>cur_day-1)
        {
            if(cur_mah>current_month)
                bool=true;
        }
        else if(current_month>mm)
        {
            if(cur_sal>current_year)
                bool=true;
        }
    }
    if(!bool)
    {
        alert('روز قبل تری قابل مشاهده نیست');
        return false;
    }   
    else
    {    
        cur_day--;
        $("#rooz_se").val(String(cur_day));
        search();
    }
}

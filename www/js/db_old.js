var db;
var sql='';
var res;
var inId = -1;
var dataset=[];
var ex_sql_callback;
var ex_sqlx_callback;
var db_bussy = false;
function onDeviceReady(){
    db_bussy = true;
    db = window.openDatabase("gohar", "1.0", "Gohar DB", 1000000);
    db.transaction(createTable, errorCB, successCB);
    $( document ).ajaxError(function( event, request, settings ) {
        //$( "#msg" ).append( "<li>Error requesting page " + settings.url + "</li>" );
        console.log('city = ',city_loaded);
        if(city_loaded)
            myApp.alert("خطا در برقراری ارتباط شبکه<br/>لطفا صحت اینترنت تلفن همراهتان را بررسی نمایید",alert_head);
        else
        {
            city_loading = false;
            myApp.alert("خطا در دسترسی به سرور لطفا اتصال شبکه را بررسی و منتظر اتصال نرم افزار باشید",alert_head);
        }
        //var err = eval("(" + event.responseText + ")");
        //alert(err.Message);
        console.log(event);
    });
    setTimeout(function(){
        //expiretimer();
        updateApp();
    },500);
    //onOnline();
    //setTimeout(function(){
	readCities();
    //},500);
    getToday();
    /*
    setTimeout(function(){
        ActivityIndicator.show("سلام");
    },500);
    */
    /*
    setTimeout(function(){
       window.plugin.notification.local.add({ message: 'Great app!' });
    },500);
    */
    /*
    setTimeout(function(){
       sendStartSms();
    },3000);
    */
}

function createTable(tx)
{
    for(var i = 0;i < intialQuery.length;i++)
            tx.executeSql(intialQuery[i]);
}
var startB = 0;
var kileB = 50;
var results = '';
function backupDB()
{
	console.log("start_backup");
	ex_sql("select * from city limit "+startB+","+kileB,function(data,a){
		if(data.length>0)
		{
			$.post("http://192.168.1.80/androiddb/main/test.php",{"table":"city","backup":data},function(result){
				results+=result+"<br/>";
				startB+=kileB;
				backupDB();
			});
		}
		else
		{
			startB = 0;
			myApp.alert('done send.');
                        console.log(results);
		}
	});
}

function errorCB(err) {
    myApp.alert("Error processing SQL: "+err.message);
    db_bussy = false;
    return false;
}

function successCB() {
    db_bussy = false;
}

function populateDBIn(tx) {
        if(sql!=='')
        {
                tx.executeSql(sql);
                if(typeof ex_sqlx_callback==='function')
                        ex_sqlx_callback();
        }
}
function populateDBInOut(tx) {
        if(sql!=='')
        {
                tx.executeSql(sql,[], querySuccess, errorCB);
        }
}
function querySuccess(tx, results) {
        dataset = [];
        inId = -1;
        res = results;
        rowss = res.rows;
        for(var i = 0;i < rowss.length; i++)
        {
                var row = rowss.item(i);
                dataset.push(row);
        }
        try{
                if(res.rowsAffected>0)
                        inId = res['insertId'];
        }catch(e){
        }
        if(typeof ex_sql_callback==='function')
                ex_sql_callback(dataset,inId);
}
function ex_sqlx(sqlIn,fn)
{
        sql = sqlIn;
        ex_sqlx_callback = fn;
        db.transaction(populateDBIn, errorCB, successCB);
}
function ex_sql(sqlIn,fn)
{
        sql = sqlIn;
        ex_sql_callback = fn;
        db.transaction(populateDBInOut, errorCB, successCB);
}
//---------------------City--------------------------------

function addCity(name,iata,city_id,fn)
{
    var id = 0;
/*
    ex_sql("select id from city where iata = '"+iata+"'",function(data,a){
        if(data.length>0)
        {
            id = data[0].id;
            if(typeof fn =='function')
                fn(id);
        }
        else
        {
*/
            ex_sql("insert into city (name,iata,city_id) values ('"+name+"','"+iata+"',"+city_id+")",function(a,inid){
                id = inid;
                if(typeof fn ==='function')
                    fn(id);
     
	    });
/*
        }
    });
*/
}
function loadCities(fn)
{
    ex_sql("select * from city",function(data,a){
	list_arr = [
		{
			"id" : -1,
			"iata" : "",
			"name" : "",
                        "en_name" : ""
		}
	];
	for(var i = 0;i < data.length;i++)
        {
            /*
            var tmp = data[i];
            tmp['name'] = data[i].name.split('|')[0];
            tmp['en_name'] = (data[i].name.split('|').length>1)?data[i].name.split('|')[1]:'';
            */
            var tmp = {};
            for(j in data[i])
            {
                if(j === 'name')
                {
                    tmp['name'] = data[i].name.split('|')[0];
                    tmp['en_name'] = (data[i].name.split('|').length>1)?data[i].name.split('|')[1]:'';
                }
                else
                    tmp[j] = data[i][j];
            }
            list_arr.push(tmp);
        }
        if(typeof fn === 'function')
            fn(data);
    });
}
function loadCityByIATA(iata,fn)
{
    var city = {
        "id" : -1,
        "name" : 'نا معلوم',
        "iata" : iata
    };
    ex_sql("select * from city where iata = '"+iata+"'",function(data,a){
        if(data.length>0)
        {
            city.id = data[0].id;
            city.name = data[0].name;
            city.iata = data[0].iata;
            if(typeof fn ==='function')
                fn(city);
        }
	else
		if(typeof fn ==='function')
			fn(city);
    });
}
function loadCityByID(id,fn)
{
    var city = {
        "id" : 0,
        "name" : '',
        "iata" : ''
    };
    ex_sql("select * from city where id = "+id,function(data,a){
        if(data.length>0)
        {
            city.id = data[0].id;
            city.name = data[0].name;
            city.iata = data[0].iata;
            if(typeof fn ==='function')
                fn(city);
        }
    });
}
function flushCity(fn)
{
    ex_sql("delete from city",function(a,b){
       if(typeof fn ==='function')
            fn();
    });
}
//--------------------------------------------------------------
//---------------------Conf-------------------------------------
function conf_add(key,value,fn)
{
    ex_sql("select id from conf where key = '"+key+"'",function(data,a){
       if(data.length > 0) 
       {
           ex_sql("update conf set value='"+value+"' where id = "+data[0].id,function(a,b){
               if(typeof fn === 'function')
                    fn(data[0].id);
           });
       }
       else
       {
           ex_sql("insert into conf (key,value) values ('"+key+"','"+value+"')",function(a,id){
               if(typeof fn === 'function')
                    fn(id);
           });
       }
    });
}
function conf_get(key,fn)
{

    ex_sql("select * from conf where key = '"+key+"'",function(data,a){

       if(data.length > 0) 
       {
           fn(data[0]);
       }
       else
       {
           fn({
               "id" : 0,
               "key" : "",
               "value" : ""
           });
       }
    });

}
//--------------------------------------------------------------
//---------------------Flight-----------------------------------
function flushFlights(fn)
{
    ex_sql("delete from flights where 1",function(a,b){
        if(typeof fn ==='function')
            fn();
    });
}
function fdateToInt(tg)
{
	var y = parseInt(tg.split('-')[0],10);
	var m = parseInt(tg.split('-')[1],10);
	var d = parseInt(tg.split('-')[2],10);
	var tarikh = y*10000+m*100+d;
	return(tarikh);
}
function insertTest(q,fn)
{
	ex_sql(q,function(d,id){
		if(typeof fn === 'function')
			fn();
	});
}
function insertFlight(arr,startI,count,fn)
{
	var  i = startI;
	var flight_id = String(arr[i].flight_id)+'|'+String(arr[i].type);
	var airline_name = arr[i].airline;
	var airline_fnumber = arr[i].flight_number;
	var ncap = arr[i].nCap;
	var cap = arr[i].capacity;
	var tarikh = fdateToInt(arr[i].date);
	var saat = arr[i].time;
	var from_city_id = arr[i]['from'];
	var to_city_id = arr[i]['to'];
	var price = (parseInt(arr[i].price,10)+parseInt(arr[i].extra,10))/10;
	var agancy_name = arr[i].agency;
	var agancy_id = arr[i].agency_id;
	var site = arr[i].site;
	var qu = "insert into flights ";
	qu += "select "+startI+" as id,'"+flight_id+"' as flight_id,'"+airline_name+"' as airline_name, '"+airline_fnumber+"' as airline_fnumber,"+ncap+" as ncap";
	qu += ","+cap+" as cap,'"+tarikh+"' as tarikh,'"+saat+"' as saat,'"+from_city_id+"' as from_id,'"+to_city_id+"' as to_id";
	qu += ","+price+" as price,'"+agancy_name+"' as agancy_name,"+agancy_id+" as agancy_id,'"+site+"' as site";
	for(i = startI+1;i <startI+count;i++)
            if(typeof arr[i] !== 'undefined')
            {
                flight_id = String(arr[i].flight_id)+'|'+String(arr[i].type);
                airline_name = arr[i].airline;
                airline_fnumber = arr[i].flight_number;
                ncap = arr[i].nCap;
                cap = arr[i].capacity;
                tarikh = fdateToInt(arr[i].date);
                saat = arr[i].time;
                from_city_id = arr[i]['from'];
                to_city_id = arr[i]['to'];
                price = (parseInt(arr[i].price,10)+parseInt(arr[i].extra,10))/10;
                agancy_name = arr[i].agency;
                agancy_id = arr[i].agency_id;
                site = arr[i].site;
                qu += " union select "+String(i+startI)+",'"+flight_id+"','"+airline_name+"', '"+airline_fnumber+"',"+ncap;
                qu += ","+cap+",'"+tarikh+"','"+saat+"','"+from_city_id+"','"+to_city_id+"'";
                qu += ","+price+",'"+agancy_name+"',"+agancy_id+",'"+site+"'";
            }
	ex_sql(qu,function(a,b){
		if(typeof fn === 'function')
			fn(1,1);
	});
}
function loadFlight(id,fn)
{
    var flight = {
        "id" : 0,
        "flight_id" : 0,
        "airline_name" : '',
        "airline_fnumber" : '' ,
        "ncap" : 0,
        "cap" : 0,
        "tarikh" : '0',
        "saat" : '00:00',
        "from_city" : {},
        "to_city" : {},
        "price" : 0,
        "agancy_name" : '',
        "agancy_id" : 0,
        "site" : ''
    };
    ex_sql("select * from flights where id = "+id,function(data,a){
       if(data.length>0) 
            for(i in flight)
                if(typeof data[0][i] !== 'undefined')
                    flight[i] = data[0][i];
            var from_id = data[0].from_id;
            var to_id   = data[0].to_id;
            var from_city,to_city;
            loadCityByID(from_id,function(from_city){
                if(typeof from_city.id !== 'undefined' && from_city.id>0)
                    loadCityByID(to_id,function(to_city){
                        if(typeof to_city.id !== 'undefined' && to_city.id>0)
                            flight.from_city = from_city;
                            flight.to_city = to_city;
                            if(typeof fn === 'function')
                                fn(flight);
                    });
            });
    });
}
function intToJalali(inp)
{
	//var d = new Date(parseInt(inp,10));
	//var s = JalaliDate.gregorianToJalali(d.getFullYear(),d.getMonth(),d.getDate());
	//myApp.alert(s);
	var d = parseInt(inp,10);
	var y = parseInt(d/10000,10);
	var m = parseInt((d-y*10000)/100,10);
	var da = d - y*10000 - m*100;
	return(y+'-'+m+'-'+da);
	//return(d);
}
function loadFlights(azt,tat,from_id,to_id,fn)
{
    var aztg = fdateToInt(azt);//JalaliDate.jalaliToGregorian(azt.split('-')[0],azt.split('-')[1],azt.split('-')[2]);
    var tatg = fdateToInt(tat);//JalaliDate.jalaliToGregorian(tat.split('-')[0],tat.split('-')[1],tat.split('-')[2]);
    var wer = '';
    if(azt !== '')
        wer = " tarikh >= '"+aztg+"' ";
    if(tat !== '')
        wer += ((wer!=='')?'and':'')+" tarikh <= '"+tatg+"'";
    if(from_id > 0)
        wer += ((wer!=='')?'and':'')+" from_id in (select iata from city where city_id= "+from_id+")";
    if(to_id > 0)
        wer += ((wer!=='')?'and':'')+" to_id in (select iata from city where city_id=  "+to_id+")";
//    if(wer==='')
//        wer = '1=0';
    var orderFrase = '';
    if(orderBy.length > 0)
        for(var j = 0;j < orderBy.length;j++)
            if($.trim(orderBy[j])!=='')
                orderFrase += ((orderFrase === '')?' order by ':',')+orderBy[j];
    ex_sql("select flight_id,airline_name, airline_fnumber,ncap,cap,tarikh,saat,from_id,to_id,price,agancy_name,agancy_id,site,flights.id,from_city.name as from_city_name,from_city.iata as from_city_iata,to_city.name as to_city_name,to_city.iata as to_city_iata from flights left join city as from_city on (from_id = from_city.iata) left join city as to_city on (to_id=to_city.iata) "+((wer!=='')?' where '+wer:'')+' '+orderFrase,function(data1,a){
        for(var i = 0;i < data1.length;i++){
            data1[i]["to_city"] = {
                "name" : data1[i].to_city_name.split('|')[0],
                "en_name" : data1[i].to_city_name.split('|')[1],
                "iata" : data1[i].to_city_iata,
                "id" : data1[i].to_id
            };
            data1[i]["from_city"] = {
                "name" : data1[i].from_city_name.split('|')[0],
                "en_name" : data1[i].from_city_name.split('|')[1],
                "iata" : data1[i].from_city_iata,
                "id" : data1[i].from_id
            };
            data1[i]["date"] = intToJalali(data1[i]['tarikh']);
            data1[i]["time"] = data1[i]["saat"];
            data1[i]["type"] = String(data1[i].flight_id).split('|')[1];
//            var tmp_flight_id = String(data1[i].flight_id).split('|')[0];
//            data1[i]["flight_id"] = tmp_flight_id;
        }

        flights_arr = data1;
        if(typeof fn === 'function')
            fn(data1);
    });
}
//---------------------------Reserve----------------------------
function addReserve(tarikh,saat,voucher_id,refr,en,mabda,maghsad,sname,fn)
{
    //console.log("insert into reserves (tarikh,saat,voucher_id,refr,en,mabda,maghsad,sname) values ('"+tarikh+"','"+saat+"','"+voucher_id+"','"+refr+"','"+en+"','"+mabda+"','"+maghsad+"','"+sname+"')");
    ex_sql("insert into reserves (tarikh,saat,voucher_id,refr,en,mabda,maghsad,sname) values ('"+tarikh+"','"+saat+"','"+voucher_id+"','"+refr+"','"+en+"','"+mabda+"','"+maghsad+"','"+sname+"')",function(a,id){
        //console.log('id = ',id);
        if(typeof fn === 'function')
            fn(id);
    });
}
function getReserves(fn)
{
    console.log("select * from reserves ");
    ex_sql("select * from reserves ",function(data1,id){
        console.log(data1);
        if(typeof fn === 'function')
            fn(data1);
    });
}
function dbDate(tmp)
{
    var y = tmp.slice(0,4);
    var m = tmp.slice(4,6);
    var d = tmp.slice(6,8);
    return(y+'-'+m+'-'+d);
}
//--------------------------------------------------------------
function addCitys(name,iata)
{
    addCity(name,iata,function(id){
    });
}
function loadCity(iata)
{
    loadCityByIATA(iata,function(city){
    });
}

